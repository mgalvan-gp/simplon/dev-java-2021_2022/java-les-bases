package test.mel.oop;

public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public int soustract(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    public int modulo(int a, int b) {
        return a % b;
    }

    public Integer calculate(int a, int b, char operator) {

        // switch
        // if (operator == ('+')) {
        //     return a + b;
        // } else if (operator == '-') {
        //     return a - b;
        // } else if (operator == '*') {
        //     return a * b;
        // } else if (operator == '/') {
        //     return a / b;
        // } else if (operator == '%') {
        //     return a % b;
        // } else {
        //     throw new Error("Ca va pas !");
        // }

        switch (operator) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b;
            case '%':
                return a % b;
            default:
                return null;
        }
    }

    public boolean isEven(int arg) {
        if (arg % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
