package test.mel.oop;

public class Bonus {

    public boolean isCorrectPassword(String str) {

        boolean string = str.length() > 4;
        boolean isOneUppercaseLettre = false;
        boolean isOneNumber = false;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i))) {
                isOneUppercaseLettre = true;
            } else if (Character.isDigit(str.charAt(i))) {
                isOneNumber = true;
            }
        }
        return (string && isOneNumber && isOneUppercaseLettre);

    }

    public boolean isCorrectEmail(String str) {
        String pattern = "^[a-zA-Z0-9-_.]+@[a-zA-Z0-9]+\\.[a-z]{2,6}$";
        return (str.matches(pattern)); // .matches("regex")
    }

}
